Spring initilizer by command line : 

curl "https://start.spring.io/starter.tgz" -d name="reactive-boot2-server-jib" -d description="Simplifying Docker server for Spring Boot 2 apps" -d bootVersion=2.0.4.RELEASE -d dependencies=actuator,web -d language=java -d type=maven-project -d baseDir=reactiveboot2-jib-server -d groupId=com.rdas.reactive.server -d artifactId=reactiveboot2-jib-server -d version=1.0-SNAPSHOT | tar -xzvf -

https://devops.datenkollektiv.de/banner.txt/index.html

https://tech.asimio.net/2018/08/10/Simplifying-packaging-Spring-Boot-2-applications-into-Docker-images-using-Google-Jib.html

https://bitbucket.org/account/user/asimio/projects/TECH
https://www.youtube.com/watch?v=H6gR_Cv4yWI
https://github.com/GoogleContainerTools/jib/issues/541


1. mvn clean package
2. mvn jib:exportDockerContext
3. cd target/jib-docker-context/
4. docker build -t spring-restserver .
5. docker run -d -p 8080:8080 --name restserver spring-restserver
6. docker logs restserver -f
7. curl localhost:8080/

TODO : 
Not working 
mvn jib:build


https://www.journaldev.com/20763/spring-webflux-reactive-programming
https://docs.spring.io/spring/docs/5.0.0.BUILD-SNAPSHOT/spring-framework-reference/html/web-reactive.html
1. https://dzone.com/articles/spring-boot-with-embedded-mongodb
2. https://dzone.com/articles/doing-stuff-with-spring-webflux
3. http://javasampleapproach.com/testing/springboot-webflux-test-webfluxtest

https://github.com/hantsy/spring-reactive-sample/blob/master/routes/src/main/java/com/example/demo/PostHandler.java
