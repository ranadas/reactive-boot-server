package com.rdas.reactive.server.reactiveweb;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

//https://blog.knoldus.com/future-vs-completablefuture-1/
//https://dzone.com/articles/20-examples-of-using-javas-completablefuture

/**
 * https://github.com/search?l=Java&o=desc&q=completable+future&s=updated&type=Repositories
 * <p>
 * https://github.com/karlkyck/spring-boot-completablefuture/blob/master/src/main/java/com/humansreadcode/example/service/UserServiceImpl.java
 * https://www.callicoder.com/java-8-completablefuture-tutorial/
 * http://www.deadcoderising.com/java8-writing-asynchronous-code-with-completablefuture/
 * <p>
 * <p>
 * https://github.com/spotify/completable-futures
 * https://github.com/ibercode/CompletableFuture
 * <p>
 * https://github.com/hemonth/completablefuture/blob/master/src/main/java/com/example/my/service/QuoteService.java
 * <p>
 * https://codereview.stackexchange.com/questions/147774/enforce-execution-order-with-collections-of-completeablefutures
 * https://blog.rapid7.com/2015/07/21/playing-with-java-8s-completable-futures/
 * https://blog.indrek.io/articles/java-8-completablefuture/
 */
@Slf4j
public class CompletableFutureTest {

    @Test
    public void runAsync() {
        CompletableFuture future = CompletableFuture.runAsync(() -> {
            try {
                System.out.println("Running asynchronous task in parallel");
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
                throw new IllegalStateException(ex);
            }
        });
        System.out.println(future.isDone());
    }

    @Test
    public void supplyAsync() {
        CompletableFuture future = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            return "This is the result of the asynchronous computation";
        });
    }


    @Test
    public void runAsyncTest() throws ExecutionException, InterruptedException {
        String result = future.get();
        System.out.println(result);
    }


    CompletableFuture<String> future = CompletableFuture.supplyAsync(new Supplier<String>() {
        @Override
        public String get() {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            return "Result of the asynchronous computation";
        }
    });

    @Test
    public void runFutures() throws Exception {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Future<String> result = executorService.submit(() -> {
            System.out.println("Start useful work");
            delay(5000); //simulate network call
            return "{\"result\":\"success\"}";
        });

        IntStream.range(1, 10).forEach(i -> {
            delay(100);
            System.out.println("Doing useful work");
        });
        System.out.println(result.get());
    }


    private void delay(int ms) {
        try {
            TimeUnit.MILLISECONDS.sleep(ms);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    @Getter
    @AllArgsConstructor
    @ToString
    public static class ToDo<T> {
        private final T id;
//        private final LocalDate creationDate;

    }

    // Simulate REST Client
    public static class RestApiClient {
        <T>ToDo getToDo(T id) {
            return new ToDo(id);
        }

        <T>List<ToDo> getToDos(List<T> ids) {
            return ids.stream()
                    .map(id -> new ToDo(id))
                    .collect(Collectors.toList());
            //return Collections.EMPTY_LIST;
        }
    }

    //@Autowired
    RestApiClient restApiClient = new RestApiClient();

    <T> CompletableFuture<List<ToDo>> getMultipleToDosAsync(List<T> ids) {
        CompletableFuture<List<ToDo>> future = CompletableFuture.supplyAsync(() -> restApiClient.getToDos(ids));
        return future;
    }

    <T> CompletableFuture<ToDo> getToDoAsync(T id) {
        CompletableFuture<ToDo> future = CompletableFuture.supplyAsync(new Supplier<ToDo>() {
            @Override
            public ToDo get() {
                final ToDo toDo = restApiClient.getToDo(id);
                return toDo;
            }
        });
        return future;
    }

    private <T> List<ToDo> processIds (Collection<List<Integer>> partitionedIds) {
        List<CompletableFuture<List<ToDo>>> futures = partitionedIds.stream()
                .map(partition -> getMultipleToDosAsync(partition))
                .collect(Collectors.toList());

        List<List<ToDo>> collect = futures.parallelStream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
        return collect.stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
        //return Collections.EMPTY_LIST;
    }

    public <T> List<ToDo> getToDos(List<T> ids) {

        List<CompletableFuture<ToDo>> futures =
                ids.parallelStream()
                        .map(id -> getToDoAsync(id))
                        .collect(Collectors.toList());

        List<ToDo> result =
                futures.parallelStream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    //http://www.codingpedia.org/ama/how-to-make-parallel-calls-in-java-with-completablefuture-example
    @Test
    public void runToDoFutures() throws Exception {
        printString.accept("printing looped String!");

        Collection<List<Integer>> partitions = partition(idCollection.get(), 25);
        log.info("\t partitioned collection {} ", partitions.toString());

        List<ToDo> toDos = processIds(partitions);
        log.info("Processed ToDos {}", toDos.toString());

//        partitions.forEach(partition-> {
//            List<ToDo> toDos = getToDos(partition);
//            System.out.println(toDos.toString());
//        });
        //List<ToDo> toDos = getToDos(strings);
    }

    private <T> Collection<List<T>> partition(List<T> list, int size) {
        final AtomicInteger counter = new AtomicInteger(0);

        return list.stream()
                .collect(Collectors.groupingBy(it -> counter.getAndIncrement() / size))
                .values();
    }
    //List<String> strings = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k");
    //List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    private Supplier<List<Integer>> idCollection = () ->
            IntStream.range(1, 1000)
            .mapToObj(m -> m)
            .collect(Collectors.toList());

    private Consumer<String> printString = (arg)->
            IntStream.range(1, 10).forEach(i -> {
            //delay(100);
            System.out.println("Doing CompletableFuture work");
    });
}
