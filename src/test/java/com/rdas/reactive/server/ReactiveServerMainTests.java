package com.rdas.reactive.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReactiveServerMainTests {

    private WebClient client = WebClient.create("http://localhost:8081");


    @Test
    public void contextLoads() {
        Mono<ClientResponse> result = client.get()
                .uri("/example")
                .accept(MediaType.TEXT_PLAIN)
                .exchange();

        String s = ">> result = " + result.flatMap(res -> res.bodyToMono(String.class)).block();
        System.out.println(s);
    }


}
