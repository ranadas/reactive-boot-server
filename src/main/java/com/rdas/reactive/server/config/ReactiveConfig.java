package com.rdas.reactive.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;

@Configuration
//step 1 : Enable EnableWebFlux
@EnableWebFlux
public class ReactiveConfig {
    //    @Bean
//    public NettyContext nettyContext(ApplicationContext context) {
//        HttpHandler handler = WebHttpHandlerBuilder.applicationContext(context).build();
//        ReactorHttpHandlerAdapter adapter = new ReactorHttpHandlerAdapter(handler);
//        HttpServer httpServer = HttpServer.create("localhost", port);
//        return httpServer.newHandler(adapter).block();
//    }
}
