package com.rdas.reactive.server.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

//https://www.vojtechruzicka.com/spring-boot-version/
@Slf4j
//todo @Configuration
public class BuildVersionConfig {
    @Autowired
    private Environment environment;

    @Autowired
    private BuildProperties buildProperties;


    @PostConstruct
    public void init() {
        log.info("Name : {}, version {}, time {}, arti {}, grpup {}"
                , buildProperties.getName(), buildProperties.getVersion(), buildProperties.getTime(),
                buildProperties.getArtifact(), buildProperties.getGroup());
    }

//    @Bean
//    public BuildProperties buildProperties() throws Exception {
//        String[] activeProfiles = environment.getActiveProfiles();
//
//        return new BuildProperties(
//                loadFrom(this.properties.getBuild().getLocation(), "build"));
//    }
}
