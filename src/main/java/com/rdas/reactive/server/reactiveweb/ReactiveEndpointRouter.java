package com.rdas.reactive.server.reactiveweb;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.contentType;

//step 2 : Add RouterFunctions for routes (aka controller endpoints)
@Configuration
public class ReactiveEndpointRouter {
    @Bean
    public RouterFunction<ServerResponse> route(RouteHandler routeHandler) {
        return RouterFunctions
                .route(RequestPredicates.GET("/hello")
                        .and(accept(MediaType.TEXT_PLAIN)), routeHandler::hello);
    }
    /*
    @Bean
    public RouterFunction<ServerResponse> route(PersonHandler personHandler) {
        return RouterFunctions.route(RequestPredicates.GET("/people/{id}").and(accept(MediaType.APPLICATION_JSON)), personHandler::get)
                .andRoute(RequestPredicates.GET("/people").and(accept(MediaType.APPLICATION_JSON)), personHandler::all)
                .andRoute(RequestPredicates.POST("/people").and(accept(MediaType.APPLICATION_JSON)).and(contentType(MediaType.APPLICATION_JSON)), personHandler::post)
                .andRoute(RequestPredicates.PUT("/people/{id}").and(accept(MediaType.APPLICATION_JSON)).and(contentType(MediaType.APPLICATION_JSON)), personHandler::put)
                .andRoute(RequestPredicates.DELETE("/people/{id}"), personHandler::delete)
                .andRoute(RequestPredicates.GET("/people/country/{country}").and(accept(MediaType.APPLICATION_JSON)), personHandler::getByCountry);
    }
    */
}
